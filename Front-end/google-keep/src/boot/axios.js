import axios from 'axios'
import VueAxios from 'vue-axios'

const axiosInstance = axios.create({
  baseURL: 'http://localhost:8000/api/v1/notes',
})

export default ({ app, router, Vue }) => {
  Vue.prototype.$axios = axiosInstance
}

Vue.use(VueAxios, axios)