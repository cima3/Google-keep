# Google Keep

## Backend

- Poner la carpeta app-laravel en este path: xampp\htdocs.
- En el panel de control de XAMPP inicializar Apache y MySQL.
- Abrir la carpeta: xampp\htdocs\app-laravel.
- En la terminal ejecutar el comando: 'composer install'
- Ejecutar el comando: 'php artisan serve'
---
## Frontend

- Abrir la carpeta: Front-end\google-keep
- En la terminal ejecutar el comando: 'npm install'
- Ejecutar el comando: 'quasar dev'