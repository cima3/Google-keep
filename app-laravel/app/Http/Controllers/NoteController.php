<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    public function index()
    {
        $notes = Note::orderBy('created_at', 'desc')
            ->orderBy('fijada', 'desc')
            ->paginate(10);

        return response()->json($notes);
    }

    public function store(Request $request)
    {
        $note = Note::create($request->all());

        return response()->json($note, 201);
    }

    public function destroy($id)
    {
        Note::destroy($id);

        return response()->json(null, 204);
    }

    public function update(Request $request, $id)
    {
        $note = Note::findOrFail($id);
        $note->update($request->all());

        return response()->json($note, 200);
    }

    public function archive(Request $request, $id)
    {
        $note = Note::findOrFail($id);
        $note->archived = true;
        $note->save();

        return response()->json($note, 200);
    }

    public function archived()
    {
        $archivedNotes = Note::where('archived', true)
            ->orderBy('created_at', 'desc')
            ->paginate(10);

        return response()->json($archivedNotes);
    }
}
