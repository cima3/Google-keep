<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Note;

class NoteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Crear algunas notas de ejemplo
        Note::create([
            'title' => 'Nota 1',
            'content' => 'Contenido de la nota 1',
            'pinned' => false,
            'archived' => false,
        ]);

        Note::create([
            'title' => 'Nota 2',
            'content' => 'Contenido de la nota 2',
            'pinned' => true,
            'archived' => false,
        ]);

        Note::create([
            'title' => 'Nota 3',
            'content' => 'Contenido de la nota 3',
            'pinned' => false,
            'archived' => true,
        ]);

        // Puedes crear más notas según tus necesidades

        // ...

    }
}
