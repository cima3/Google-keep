<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

use App\Http\Controllers\NoteController;

Route::group(['prefix' => 'v1'], function () {
    Route::get('/notes', [NoteController::class, 'index']);
    Route::post('/notes', [NoteController::class, 'store']);
    Route::delete('/notes/{id}', [NoteController::class, 'destroy']);
    Route::put('/notes/{id}', [NoteController::class, 'update']);
    Route::post('/notes/{id}/archived', [NoteController::class, 'archive']);
    Route::get('/notes/archived', [NoteController::class, 'archived']);
});
